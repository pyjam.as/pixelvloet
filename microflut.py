import socket
import struct
import random
from pathlib import Path
from PIL import Image

ip = "151.216.14.69"
port = 1337

sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
sock.connect((ip, port))
scrwidth, scrheight = 160, 80
xoff, yoff = 0, 0


def packet(x, y, r, g, b):
    return f"PX {x} {y} {r:02x}{g:02x}{b:02x}\n".encode("utf8")

def snek():
    frames = []
    for path in Path("./snek_small").glob("*.png"):
        print(path)
        image = Image.open(path).convert("RGB")
        pixels = image.load()
        imgwidth, imgheight = image.size
        frame = [packet(x+xoff, y+yoff, *pixels[x, y])
                 for x in range(min([imgwidth, scrwidth]))
                 for y in range(min([imgheight, scrheight]))
                 if pixels[x,y] != (0, 255, 0)]
        random.shuffle(frame)
        frames.append(b"".join(frame))
    return frames


frames = snek()
while True:
    for p in frames:
        sock.send(p)
