import socket
import random
from struct import pack
from PIL import Image

IP = "100.65.0.2"
PORT = 5005

image = Image.open("rasmus.jpg")
xoff, yoff = 1300, 500

sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

width, height = image.size
pixels = image.load()


def to_coord(num):
    return pack('BB', num & 0xFF, num >> 8)


def packet(x, y, r, g, b):
    result = bytes([0, 0])
    result += bytes([*to_coord(x), *to_coord(y), r, g, b])
    return result


def imagepacket(x, y):
    r, g, b = pixels[x,y]
    result = packet(x + xoff, y + yoff, r, g, b)
    return result


packets = [imagepacket(x, y)
           for x in range(width)
           for y in range(height)]
random.shuffle(packets)

while True:
    for p in packets:
        sock.sendto(p, (IP, PORT))
